# django-vue


## Installation
Necesitas tener instalado `python3`, `virtualenv`, `node v12.x.x` y `yarn` para correr este proyecto.
1. Crear tu entorno virtual con `virtualenv env --python=python3`
2. Activa el mismo con `source env/bin/activate`
3. Instalá las dependencias con `pip install -r requirements.txt`
4. Prepará la db con `python3 manage.py makemigrations`
5. Ejecutá las migraciones con `python3 manage.py migrate`
5. Ejecutá el backend con `python3 manage.py runserver`
6. Abrí una nueva consola para y parate en la carpeta `frontend`
7. Instala las dependencias ejecutando `yarn`
8. Levanta el frontend con `yarn dev`
